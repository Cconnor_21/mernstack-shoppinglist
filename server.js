const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('config');
//db config
const db = config.get('mongoURI');

const items = require('./routes/api/items')

const app = express();

//body parser middleware
app.use(bodyParser.json());

//use routes
app.use('/api/items', items);

const startMongo = async () => {
  try{
    await mongoose.connect(db, {
      useNewUrlParser:true,
      useCreateIndex:true,
      useFindAndModify:false})
    console.log('MongoDB connected...');
  }
  catch(err){
    console.log(err.message);
    process.exit(1);
  }
}

startMongo();

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server Started on port ${port}`));
