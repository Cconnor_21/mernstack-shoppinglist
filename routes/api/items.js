const express = require('express');
const router = express.Router();

//Item model
const Item = require('../../models/Item');

//@route GET api/items
//@desc Get All items
//@access public
router.get('/', async (req, res) => {
  try{
    const items = await Item.find().sort({date: -1});
    res.json(items);
  }catch(err){
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

//@route POST api/items
//@desc Create an item
//@access public
router.post('/', async (req, res) => {
  try{
    const newItem = new Item({
      name: req.body.name
    });
    await newItem.save();
    res.json(newItem);
  }
  catch(err){
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

//@route DELETE api/items/:id
//@desc Delete an item
//@access public
router.delete('/:id', async (req, res) => {
    try{
      const item = Item.findById(req.params.id);
      await item.remove();
      res.json({success: true});
    }
    catch(err){
      console.error(err.message);
      res.status(404).send('Server Error');
    }
});




module.exports =  router;
